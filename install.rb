#!/usr/bin/env ruby
# coding: utf-8

f = File.new('ergasia.sh', 'w')
f.write("#!/bin/bash\n")
f.write("cd #{File.join(Dir.getwd,'src')}\n")
f.write("ruby ergasia.rbw\n")
f.chmod(0770)
f.close

f = File.new(File.join(Dir.home,'.local/share/applications/ergasia.desktop'), 'w')
f.write("#!/usr/bin/env xdg-open\n\n")
f.write("[Desktop Entry]\n")
f.write("Encoding=UTF-8\n")
f.write("Name=Ergasia\n")
f.write("Name[fr]=Ergasia\n")
f.write("Name[fr_FR]=Ergasia\n")
f.write("Exec=#{File.join(Dir.getwd,'ergasia.sh')}\n")
f.write("Icon=#{File.join(Dir.getwd,'src/resources/icons/icon.png')}\n")
f.write("Terminal=false\n")
f.write("Type=Application\n")
f.write("StartupNotify=true\n")
f.write("Categories=Office\n")
f.chmod(0770)
f.close
