# coding: utf-8

class MainWindow < Gtk::Window
	
	def initialize level
	
		super
		
		signal_connect( "destroy" ) { Gtk.main_quit }
		
		set_title GLib.application_name
		
		set_window_position :center
		set_default_size 680, 300
		set_icon( "./resources/icons/icon.png" )
		
		@fichier_config = "../configuration.yaml"
		@conf = YAML.load_file(@fichier_config)
		
		DB.establish_connection(@conf["connexion"])
		
		@info1 = grab_info 1
		@info2 = grab_info 2
		
		@toolbargen = ToolBarGen.new self
		@login = Login.new self
		
		@vbox = Gtk::Box.new :vertical
		@vbox.pack_start @toolbargen, :expand => false, :fill => false, :padding => 2
		@vbox.pack_start @login, :expand => false, :fill => false, :padding => 2
		@login.focus
		
		@actions = ActionList.new self
		@nouveau = NouveauTravail.new self
		@nouvelle_action = NouvelleAction.new self
		@listetravaux = ListeTravaux.new self
		@listeusers = ListeUsers.new self
		@utilisateur = NouvelUtilisateur.new self
		
		add @vbox
		
		show_all
	
	end
	
	def active_menu
	
		@toolbargen.each { |child|
			child.sensitive = true
		}
		
		# If not a admin, config menu is hidden
		@toolbargen.children[7].sensitive = false unless @login.get_user.admin
	
	end
	
	def efface_vbox_actuelle
		@vbox.remove @vbox.children[@vbox.children.count-1] if @vbox.children.count.eql?(2)
	end
	
	def affiche vbox
		efface_vbox_actuelle
		@vbox.pack_start vbox, :expand => true, :fill => true, :padding => 3
    self.show_all
	end
	
	def get_nouveau
		@nouveau
	end
	
	def get_login
		@login
	end
	
	def get_listetravaux
		@listetravaux
	end
	
	def get_conf
		@conf
	end
	
	def get_actions
		@actions
	end
	
	def get_nouvelle_action
		@nouvelle_action
	end
	
	def get_listeusers
		@listeusers
	end
	
	def get_utilisateur
		@utilisateur
	end
	
	def quit
		
		dialog = Gtk::MessageDialog.new(:parent => self, 
                                		:flags => :destroy_with_parent,
                                		:type => :question,
                                		:buttons_type => :yes_no,
                                		:message => "Voulez-vous réellement quitter Ergasia ?")
		response = dialog.run
		case response
		  when Gtk::ResponseType::YES
				Gtk.main_quit
		end 
		dialog.destroy
		
	end
	
	def grab_info value
	
		info = ""
		
		res = Configuration.where(:libelle => "info#{value}").first
		
		info = (res.active==1 ? res.value : "")
		
		return info
	
	end
	
	def get_info value
	
		case value
			when 1
				return @info1
			when 2
				return @info2
		end
	
	end
	
	def message_config
	
		info1_libelle = ""
		info2_libelle = ""
		changement_infos = false
		
		dialog = Gtk::Dialog.new(:title => "Configuration",
                             :parent => @window,
                             :flags => :destroy_with_parent,
                             :buttons => [[ Gtk::Stock::OK, :ok ],[ Gtk::Stock::CANCEL, :cancel ]]
                             )

		vbox = Gtk::Box.new :vertical, 2
		hboxserver = Gtk::Box.new :horizontal, 2
		hboxdb_type = Gtk::Box.new :horizontal, 2
		hboxdb = Gtk::Box.new :horizontal, 2
		hboxdb_user = Gtk::Box.new :horizontal, 2
		hboxdb_password = Gtk::Box.new :horizontal, 2
		hboxdefault_user = Gtk::Box.new :horizontal, 2
		hboxdefault_user_password = Gtk::Box.new :horizontal, 2
		framedb = Gtk::Frame.new "Configuration du serveur MySQL"
		vboxdb = Gtk::Box.new :vertical, 2
		vboxdb.add hboxdb_type
		vboxdb.add hboxserver
		vboxdb.add hboxdb
		vboxdb.add hboxdb_user
		vboxdb.add hboxdb_password
		framedb.add vboxdb
		vbox.add framedb
		frameuser = Gtk::Frame.new "Utilisateur par défaut"
		vboxuser = Gtk::Box.new :vertical, 2
		vboxuser.add hboxdefault_user
		vboxuser.add hboxdefault_user_password
		frameuser.add vboxuser
		vbox.add frameuser
		frameinfo = Gtk::Frame.new "Configuration des informations de base"
		vboxinfo = Gtk::Box.new :vertical, 2
		hboxinfo1 = Gtk::Box.new :horizontal, 2
		hboxinfo2 = Gtk::Box.new :horizontal, 2
		vboxinfo.add hboxinfo1
		vboxinfo.add hboxinfo2
		frameinfo.add vboxinfo
		
		
			
		res = Configuration.where("libelle='info1' OR libelle='info2'").order(:libelle)
	
		res.each { |h| 
			if info1_libelle.empty? then
				info1_libelle = h.value
			else
				info2_libelle = h.value
			end
		}
	
		vbox.pack_start frameinfo, :expand => true, :fill => true, :padding => 3
		
		
		
		hboxdb_type.pack_start( Gtk::Label.new( "Type de serveur :" ), :expand => false, :fill => false, :padding => 3 )
		db_type = Gtk::ComboBoxText.new
		#db_type.append_text "mysql"
		db_type.append_text "postgresql"
		db_type.active=0
		hboxdb_type.pack_start( db_type, :expand => false, :fill => false, :padding => 3 )
		
		hboxserver.pack_start( Gtk::Label.new( "Adresse du serveur :" ), :expand => false, :fill => false, :padding => 3 )
		serveur = Gtk::Entry.new
		serveur.text = @conf["connexion"]["host"]
		hboxserver.pack_start( serveur, :expand => false, :fill => false, :padding => 3 )
		
		hboxdb.pack_start( Gtk::Label.new( "Nom de la base de données :" ), :expand => false, :fill => false, :padding => 3 )
		db = Gtk::Entry.new
		db.text = @conf["connexion"]["database"]
		hboxdb.pack_start( db, :expand => false, :fill => false, :padding => 3 )
		
		hboxdb_user.pack_start( Gtk::Label.new( "Utilisateur de la base de données :" ), :expand => false, :fill => false, :padding => 3 )
		db_user = Gtk::Entry.new
		db_user.text = @conf["connexion"]["username"]
		hboxdb_user.pack_start( db_user, :expand => false, :fill => false, :padding => 3 )
		
		hboxdb_password.pack_start( Gtk::Label.new( "Mot de passe de la base de données :" ), :expand => false, :fill => false, :padding => 3 )
		db_password = Gtk::Entry.new
		db_password.text = @conf["connexion"]["password"]
		db_password.visibility = false
		hboxdb_password.pack_start( db_password, :expand => false, :fill => false, :padding => 3 )
		
		hboxdefault_user.pack_start( Gtk::Label.new( "Utilisateur par défaut :" ), :expand => false, :fill => false, :padding => 3 )
		default_user = Gtk::Entry.new
		default_user.text = @conf["divers"]["default_user"]
		hboxdefault_user.pack_start( default_user, :expand => false, :fill => false, :padding => 3 )
		
		hboxdefault_user_password.pack_start( Gtk::Label.new( "Mot de passe par défaut :" ), :expand => false, :fill => false, :padding => 3 )
		default_user_password = Gtk::Entry.new
		default_user_password.text = @conf["divers"]["default_user_password"]
		default_user_password.visibility = false
		hboxdefault_user_password.pack_start( default_user_password, :expand => false, :fill => false, :padding => 3 )
		
		hboxinfo1.pack_start( Gtk::Label.new( "Libellé de l'information n°1 :" ), :expand => false, :fill => false, :padding => 3 )
		info1 = Gtk::Entry.new
		info1.text = info1_libelle
		info1.signal_connect ("insert_text") { changement_infos = true }
		info1.signal_connect ("backspace") { changement_infos = true }
		hboxinfo1.pack_start( info1, :expand => false, :fill => false, :padding => 3 )
		
		hboxinfo2.pack_start( Gtk::Label.new( "Libellé de l'information n°2 :" ), :expand => false, :fill => false, :padding => 3 )
		info2 = Gtk::Entry.new
		info2.text = info2_libelle 
		info2.signal_connect ("insert_text") { changement_infos = true }
		info2.signal_connect ("backspace") { changement_infos = true }
		hboxinfo2.pack_start( info2, :expand => false, :fill => false, :padding => 3 )
		
		
		dialog.vbox.add(vbox)
		vbox.show_all
		dialog.run { |response| 
			if response==Gtk::ResponseType::OK then
				
				@conf["connexion"]["adapter"] = db_type.active_text
				@conf["connexion"]["host"] = serveur.text
				@conf["connexion"]["database"] = db.text
				@conf["connexion"]["username"] = db_user.text
				@conf["connexion"]["password"] = db_password.text
				@conf["divers"]["default_user"] = default_user.text
				@conf["divers"]["default_user_password"] = default_user_password.text
				
			
				output = File.new(@fichier_config, 'w')
				output.puts YAML.dump(@conf)
				output.close
			
				
				if changement_infos then
					conf1 = Configuration.where(:libelle => 'info1').first
					conf1.value = info1.text
					conf1.save
					
					conf2 = Configuration.where(:libelle => 'info2').first
					conf2.value = info2.text
					conf2.save
					
					p "Changement des libellés des infos"
				end
				
			else
				@quitconfig = true
			end
			
			dialog.destroy
		}	
	
	end

end
