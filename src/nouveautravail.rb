# coding: utf-8

class NouveauTravail < Gtk::Box

	def initialize window
	
		super(:vertical, 3)
		
		@envoi_mail = true
		
		set_border_width 10
		
		@window = window
		@conf = @window.get_conf
		@login = window.get_login
		
		@info1 = Gtk::Entry.new
		@info2 = Gtk::Entry.new
		@description = Editeur.new
		@datecreation = Gtk::Calendar.new
		@dateecheance = Gtk::Calendar.new
		@dateecheance.select_month Date.today.month+1, Date.today.year
		@valider = Gtk::Button.new :stock_id => Gtk::Stock::OK
		@annuler = Gtk::Button.new :stock_id => Gtk::Stock::CANCEL
		
		agencement
		
		@valider.signal_connect( "clicked" ) { validate }
		@annuler.signal_connect( "clicked" ) { cancel }
		
	end
	
	def serializeTextBuffer(txtbuffer)
		serialFormat = txtbuffer.register_deserialize_target(nil)
		data = txtbuffer.serialize(txtbuffer, serialFormat, txtbuffer.start_iter, txtbuffer.end_iter)
		return data
	end
	
	def deserializeTextBuffer(data, tagtable)
		txtbuffer = Gtk::TextBuffer.new tagtable
		serialFormat = txtbuffer.register_deserialize_target(nil)
		txtbuffer.deserialize(txtbuffer, serialFormat, txtbuffer.start_iter, data)
		return txtbuffer
	end
	
	def agencement
	
		hbox1 = Gtk::Box.new :horizontal, 2
		
		if !@window.get_info(1).empty? then
			vboxinfo = Gtk::Box.new :vertical, 2
			align1info1 = Gtk::Alignment.new 0, 1, 0, 0
			align2info1 = Gtk::Alignment.new 0, 0, 1, 0
			align1info1.add Gtk::Label.new( @window.get_info 1 )
			align2info1.add @info1
			vboxinfo.add align1info1
			vboxinfo.add align2info1
		end
		
		if !@window.get_info(2).empty? then
			align1info2 = Gtk::Alignment.new 0, 1, 0, 0
			align2info2 = Gtk::Alignment.new 0, 0, 1, 0
			align1info2.add Gtk::Label.new( @window.get_info 2 )
			align2info2.add @info2
			vboxinfo.add align1info2
			vboxinfo.add align2info2
		end
		
		aligndesc = Gtk::Alignment.new 0, 0, 0, 0
		aligndesc.add Gtk::Label.new( "Description :" )
		vboxinfo.pack_start( aligndesc, :expand => false, :fill => false, :padding => 3 )
		vboxinfo.pack_start( @description, :expand => true, :fill => true, :padding => 3 )
		
		hbox1.pack_start( vboxinfo, :expand => true, :fill => true, :padding => 3 )
		
		vbox1 = Gtk::Box.new :vertical, 2
		vbox1.add Gtk::Label.new( "Date de création :" )
		vbox1.add @datecreation
		vbox1.add Gtk::Label.new( "Date d'échéance :" )
		vbox1.add @dateecheance
		hbox1.pack_start( vbox1, :expand => false, :fill => false, :padding => 3 )
		
		hbox3 = Gtk::Box.new :horizontal, 2
		align1 = Gtk::Alignment.new 1, 1, 0, 0
		hbox3.pack_start( @annuler, :expand => true, :fill => true, :padding => 3 )
		hbox3.pack_start( @valider, :expand => true, :fill => true, :padding => 3 )
		align1.add hbox3
		
		vboxframe = Gtk::Box.new :vertical, 2
		vboxframe.pack_start hbox1, :expand => true, :fill => true, :padding => 3
		
		frame = Gtk::Frame.new "Nouveaux travaux"
		frame.add vboxframe
		
		pack_start frame, :expand => true, :fill => true, :padding => 3
		pack_start align1, :expand => false, :fill => false, :padding => 3
	
	end
	
	def validate
		
		datedeb = @datecreation.year.to_s + "-" + (@datecreation.month+1).to_s + "-" + @datecreation.day.to_s
		datefin = @dateecheance.year.to_s + "-" + (@dateecheance.month+1).to_s + "-" + @dateecheance.day.to_s
		
		dateok = !( @dateecheance.year<=@datecreation.year and  @dateecheance.month<=@datecreation.month and @dateecheance.day<@datecreation.day )
		
		if !@info1.text.empty? and !@description.get_text.empty? and dateok then

			tag = @description.serialize
						
			Travaux.create :info1 => @info1.text, :info2 => @info2.text, :description => @description.get_text, :description_tag => tag, :users_id => @login.get_user.id, :datedeb => datedeb, :datefin => datefin

			if @envoi_mail
				message = @window.get_info(1) + " : " + @info1.text + "\n" + @window.get_info(2) + " : " + @info2.text + "\n" + @description.get_text
				Thread.new {
					begin
						#to = ["sophie.maisonneuve@accord-immobilier.re"]
						to = ["mail@mithril.re"]
						mail to, {:from => @login.get_user.email, 
											:from_alias => "#{@login.get_user.firstname} #{@login.get_user.lastname}", 
											:subject => "#{@login.get_user.firstname} #{@login.get_user.lastname} vient d'inscire un nouveau travail dans Ergasia.", 
											:body => message, 
											:address => @conf["mail"]["smtp"], 
											:port => @conf["mail"]["port"],
											:user => @conf["mail"]["user"],
						 				  :secret => @conf["mail"]["password"],
											:ssl => @conf["mail"]["ssl"].eql?(1)
										 }
					rescue
						p "L'envoi de mail à échoué"
					end
				}
			end
			
			quit 1
		else
			erreur = ""
			erreur += "Le champs #{@window.get_info(1)} n'est pas renseigné.\n" if @info1.text.empty?
			erreur += "La description n'est pas renseignée.\n" if @description.get_text.empty?
			erreur += "La date d'échéance est antérieure à la date de création.\n" if !dateok
			dialog = Gtk::MessageDialog.new :parent => @window, :flags => :modal, :type => :error, :buttons_type => :close, :message => erreur
			dialog.title = "Erreur"
			dialog.signal_connect('response') { dialog.destroy }
    		dialog.run
		end
	
	end
	
	def cancel
	
		quit
	
	end
	
	def quit statut=nil
	
		@info1.text = ""
		@info2.text = ""
		@description.set_text ""
		@dateecheance.select_month Date.today.month+1, Date.today.year
		@dateecheance.select_day Date.today.day
		@datecreation.select_month Date.today.month, Date.today.year
		@datecreation.select_day Date.today.day
		@window.affiche @window.get_listetravaux
		@window.get_listetravaux.refresh statut unless statut.nil?
	
	end
	
end
