# coding: utf-8

class NouvelUtilisateur < Gtk::Box

	def initialize window
	
		super(:vertical, 3)
		
		set_border_width 10
		
		@window = window
		@login = window.get_login
		
		@id_user = -1
		
		@firstname = Gtk::Entry.new
		@lastname = Gtk::Entry.new
		@username = Gtk::Entry.new
		@password = Gtk::Entry.new
		@password.visibility = false
		@admin = Gtk::CheckButton.new
		@active = Gtk::CheckButton.new
		@active.active = true

		@valider = Gtk::Button.new :stock_id => Gtk::Stock::OK
		@annuler = Gtk::Button.new :stock_id => Gtk::Stock::CANCEL
		
		agencement
		
		@valider.signal_connect( "clicked" ) { validate }
		@annuler.signal_connect( "clicked" ) { cancel }
	
	end
	
	def agencement
	
		hbox2 = Gtk::Box.new :horizontal, 2
		
		vbox2 = Gtk::Box.new :vertical, 2
		align3 = Gtk::Alignment.new 1, 0, 1, 0
		align3.add vbox2
		vbox2.add Gtk::Label.new( "Prénom :" )
		vbox2.add @firstname
		hbox2.pack_start( align3, :expand => true, :fill => true, :padding => 3 )

		vbox2 = Gtk::Box.new :vertical, 2
		align3 = Gtk::Alignment.new 1, 0, 1, 0
		align3.add vbox2
		vbox2.add Gtk::Label.new( "Nom :" )
		vbox2.add @lastname
		hbox2.pack_start( align3, :expand => true, :fill => true, :padding => 3 )
		
		hbox3 = Gtk::Box.new :horizontal, 2
		align1 = Gtk::Alignment.new 1, 1, 0, 0
		hbox3.pack_start( @annuler, :expand => true, :fill => true, :padding => 3)
		hbox3.pack_start( @valider, :expand => true, :fill => true, :padding => 3)
		align1.add hbox3
		
		vboxframe = Gtk::Box.new :vertical, 2
		vboxframe.pack_start hbox2, :expand => false, :fill => false, :padding => 3
		
		hboxusername = Gtk::Box.new :horizontal, 2
		hboxusername.pack_start( Gtk::Label.new( "Nom d'utilisateur :" ), :expand => false, :fill => false, :padding => 3 )
		hboxusername.pack_start( @username, :expand => false, :fill => false, :padding => 3 )
		
		vboxframe.pack_start hboxusername, :expand => false, :fill => false, :padding => 3
		
		hboxmdp = Gtk::Box.new :horizontal, 2
		hboxmdp.pack_start( Gtk::Label.new( "Mot de passe :" ), :expand => false, :fill => false, :padding => 3 )
		hboxmdp.pack_start( @password, :expand => false, :fill => false, :padding => 3 )
		
		vboxframe.pack_start hboxmdp, :expand => false, :fill => false, :padding => 3
		
		hboxadmin = Gtk::Box.new :horizontal, 2
		hboxadmin.pack_start( Gtk::Label.new( "Administrateur ? " ), :expand => false, :fill => false, :padding => 3 )
		hboxadmin.pack_start( @admin, :expand => false, :fill => false, :padding => 3 )
		
		vboxframe.pack_start hboxadmin, :expand => false, :fill => false, :padding => 3
		
		hboxactif = Gtk::Box.new :horizontal, 2
		hboxactif.pack_start( Gtk::Label.new( "Actif ? " ), :expand => false, :fill => false, :padding => 3 )
		hboxactif.pack_start( @active, :expand => false, :fill => false, :padding => 3)
		
		vboxframe.pack_start hboxactif, :expand => false, :fill => false, :padding => 3
		
		@frame = Gtk::Frame.new "Nouvel utilisateur"
		@frame.add vboxframe
		
		pack_start @frame, :expand => true, :fill => true, :padding => 3
		pack_start align1, :expand => false, :fill => false, :padding => 3
	
	end
	
	def validate
		
		if ( !@firstname.text.empty? and !@lastname.text.empty? and !@username.text.empty? and ( !@password.text.empty? or @id_user>0 ) ) then
			
			active = ( @active.active? ? 1 : 0 )
			admin = ( @admin.active? ? 1 : 0 )
			u = nil			
			if @id_user>0 then
				u = Users.find(@id_user)
			else
				u = Users.new
			end		
			u.firstname= @firstname.text
			u.lastname=@lastname.text
			u.username=@username.text
			u.password=BCrypt::Password.create(@password.text) unless @password.text.empty?
			u.admin= admin.to_s
			u.active=active.to_s
			u.save
			quit 1
			
		else
			erreur = ""
			erreur += "Le prénom n'est pas renseigné.\n" if @firstname.text.empty?
			erreur += "Le nom n'est pas renseigné.\n" if @lastname.text.empty?
			erreur += "Le nom d'utilisateur n'est pas renseigné.\n" if @username.text.empty?
			erreur += "Le mot de passe n'est pas renseigné.\n" if ( @password.text.empty? and @id_user<0 )
			dialog = Gtk::MessageDialog.new @window, Gtk::Dialog::MODAL, Gtk::MessageDialog::ERROR, Gtk::MessageDialog::BUTTONS_CLOSE, erreur
			dialog.title = "Erreur"
			dialog.signal_connect('response') { dialog.destroy }
    		dialog.run
		end
	
	end
	
	def cancel
	
		quit
	
	end
	
	def quit statut=nil
	
		@id_user = -1
		@firstname.text = ""
		@lastname.text = ""
		@username.text = ""
		@admin.active = false
		@active.active = true
		
		@window.get_listeusers.remplir_users unless statut.nil?
		@window.affiche @window.get_listeusers
	
	end
	
	def change id
		@id_user = id
		
		if @id_user>0 then
			h = Users.find(id)
			
			@firstname.text = h.firstname
			@lastname.text = h.lastname
			@username.text = h.username
			@admin.active = h.admin.to_i.eql?(1)
			@active.active = h.active.to_i.eql?(1)

		end
		
		@frame.label = (@id_user>0 ? "Modifier utilisateur" : "Nouvel utilisateur")
	end
	
end
