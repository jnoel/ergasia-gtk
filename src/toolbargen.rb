# coding: utf-8

class ToolBarGen < Gtk::Toolbar

	def initialize window
	
		super()
		
        set_toolbar_style Gtk::Toolbar::Style::BOTH
		
		newtb = Gtk::ToolButton.new( :icon_widget => Gtk::Image.new( :file => "./resources/icons/new.png" ), :label => "Nouveau" )
        travauxtb = Gtk::ToolButton.new( :icon_widget => Gtk::Image.new( :file => "./resources/icons/start.png" ), :label => "En cours" )
        latetb = Gtk::ToolButton.new( :icon_widget => Gtk::Image.new( :file => "./resources/icons/late.png" ), :label => "En retard" )
        donetb = Gtk::ToolButton.new( :icon_widget => Gtk::Image.new( :file => "./resources/icons/attachment.png" ), :label => "Classés" )
        usertb = Gtk::ToolButton.new( :icon_widget => Gtk::Image.new( :file => "./resources/icons/users22.png" ), :label => "Utilisateurs" )
        configtb = Gtk::ToolButton.new( :icon_widget => Gtk::Image.new( :file => "./resources/icons/config.png" ), :label => "Configuration" )
        abouttb = Gtk::ToolButton.new( :icon_widget => Gtk::Image.new( :file => "./resources/icons/help22.png" ), :label => "A Propos" )
        quittb = Gtk::ToolButton.new( :icon_widget => Gtk::Image.new( :file => "./resources/icons/exit.png" ), :label => "Quitter" )
        printtb = Gtk::ToolButton.new( :icon_widget => Gtk::Image.new( :file => "./resources/icons/print.png" ), :label => "Imprimer" )
        
  		printtb.signal_connect( "clicked" ) { 
  			# TODO
  		}
  		
  		configtb.signal_connect( "clicked" ) {
  			window.message_config
  		}
  		
  		abouttb.signal_connect( "clicked" ) { 
  			about = About.new 
  			about.signal_connect('response') { about.destroy }
  		}
        
        quittb.signal_connect( "clicked" ) { 
        	window.quit
        }
        usertb.signal_connect( "clicked" ) { 
			window.affiche window.get_listeusers
        }
        newtb.signal_connect( "clicked" ) { 
			window.affiche window.get_nouveau
        }
        travauxtb.signal_connect( "clicked" ) { 
			window.affiche window.get_listetravaux
			window.get_listetravaux.refresh 1
        }
        latetb.signal_connect( "clicked" ) { 
			window.affiche window.get_listetravaux
			window.get_listetravaux.refresh 2
        }
        donetb.signal_connect( "clicked" ) { 
			window.affiche window.get_listetravaux
			window.get_listetravaux.refresh 3
        }
        
        tool = [newtb, Gtk::SeparatorToolItem.new, travauxtb, latetb, donetb, Gtk::SeparatorToolItem.new, usertb, configtb, Gtk::SeparatorToolItem.new, abouttb, quittb]
        
        tool.each_index { |i| 
        	self.insert tool[i],i
        	tool[i].sensitive = false unless ( i.eql?( tool.count-1 ) or i.eql?( tool.count-2 ) )
        }
	
	end

end
