# coding: utf-8

class ActionList < Gtk::Box

	def initialize window
	
		super(:vertical)
		
		set_border_width 10
		
		@window = window
		@login = window.get_login
		@id_travaux = -1
		@user = @window.get_login.get_user 
		
		agencement
	
	end
	
	def agencement
	
		vbox = Gtk::Box.new :vertical
				
		@info1 = Gtk::Entry.new
		@info1.editable = false
		@info1.signal_connect ("insert_text") { @modifavancement.sensitive = true }
		@info1.signal_connect ("backspace") { @modifavancement.sensitive = true }
		@info2= Gtk::Entry.new
		@info2.editable = false
		@info2.signal_connect ("insert_text") { @modifavancement.sensitive = true }
		@info2.signal_connect ("backspace") { @modifavancement.sensitive = true }
		@description = Editeur.new
		@description.notepad.editable = false
		@description.notepad.buffer.signal_connect ("changed") { @modifavancement.sensitive = true }
		@avancement = Gtk::Scale.new(:horizontal, 0, 100, 5)
		@avancement.signal_connect ("value_changed") { @modifavancement.sensitive = true }
		@dateecheance = Gtk::Calendar.new
		@dateecheance.signal_connect ("day-selected") { @modifavancement.sensitive = true }
		@datecreation = ""
		@icone1 = Gtk::ComboBox.new :model => Gtk::ListStore.new(String, Gdk::Pixbuf)
		renderer_pix = Gtk::CellRendererPixbuf.new		
		@icone1.pack_start(renderer_pix, true)
		@icone1.set_attributes(renderer_pix, :pixbuf => 1)
		@icone2 = Gtk::ComboBox.new :model => Gtk::ListStore.new(String, Gdk::Pixbuf)
		@icone2.pack_start(renderer_pix, true)
		@icone2.set_attributes(renderer_pix, :pixbuf => 1)
	
		@frametravail = Gtk::Frame.new
		@frametravail.add info_travail
		@frametravail.label = "Travail"
		
		vbox.pack_start( @frametravail, :expand => false, :fill => false, :padding => 3 ) 
		
		vboxaction = Gtk::Box.new :vertical
		vboxaction.pack_start( boutons, :expand => false, :fill => false, :padding => 3 ) 
		vboxaction.pack_start( tableau_travaux, :expand => true, :fill => true, :padding => 3 ) 
		frameaction = Gtk::Frame.new
		frameaction.add vboxaction
		frameaction.label = "Liste des actions"
		
		vbox.pack_start( frameaction, :expand => true, :fill => true, :padding => 3 )
		
		self.pack_start vbox, :expand => true, :fill => true, :padding => 3
	
	end
	
	def info_travail
	
		vbox = Gtk::Box.new :vertical
		hbox1 = Gtk::Box.new :horizontal
		vboxinfo = Gtk::Box.new :vertical
		
		if !@window.get_info(1).empty? then
			align1info1 = Gtk::Alignment.new 0, 1, 0, 0
			align2info1 = Gtk::Alignment.new 0, 0, 1, 0
			align1info1.add Gtk::Label.new( @window.get_info 1 )
			align2info1.add @info1
			vboxinfo.add align1info1
			vboxinfo.add align2info1
		end
		
		if !@window.get_info(2).empty? then
			align1info2 = Gtk::Alignment.new 0, 1, 0, 0
			align2info2 = Gtk::Alignment.new 0, 0, 1, 0
			align1info2.add Gtk::Label.new( @window.get_info 2 )
			align2info2.add @info2
			vboxinfo.add align1info2
			vboxinfo.add align2info2
		end
		
		hbox1.pack_start( vboxinfo, :expand => true, :fill => true, :padding => 3 )
		
		vboxdate = Gtk::Box.new :vertical
		aligndate = Gtk::Alignment.new 0, 1, 0, 0
		aligndate.add Gtk::Label.new "Date d'échéance :"
		vboxdate.pack_start( aligndate, :expand => false, :fill => false, :padding => 3 )
		vboxdate.pack_start( @dateecheance, :expand => false, :fill => false, :padding => 3 )
		
		hbox1.pack_start( vboxdate, :expand => false, :fill => false, :padding => 3 )
		
		vboxavancement = Gtk::Box.new :vertical
		align1avancement = Gtk::Alignment.new 0, 1, 0, 0
		align2avancement = Gtk::Alignment.new 0, 0, 1, 0
		align1avancement.add Gtk::Label.new( "Avancement :" )
		align2avancement.add @avancement
		vboxavancement.add align1avancement
		vboxavancement.add align2avancement
		hbox1.pack_start( vboxavancement, :expand => false, :fill => false, :padding => 3 )
		
		@modifavancement = Gtk::Button.new :stock_id => Gtk::Stock::APPLY
		@modifavancement.signal_connect ("clicked") {
			date = @dateecheance.year.to_s + "-" #+ (@dateecheance.month+1).to_s + "-" + @dateecheance.day.to_s
			date += ( (@dateecheance.month+1).to_s.size.eql?(1) ? (@dateecheance.month+1).to_s.insert(0, '0') : (@dateecheance.month+1).to_s ) + "-"
			date += ( @dateecheance.day.to_s.size.eql?(1) ? @dateecheance.day.to_s.insert(0, '0') : @dateecheance.day.to_s )
			datefin = Date.parse(date)
			modif_travail @id_travaux, @info1.text, @info2.text, @description.get_text, @description.serialize, @avancement.value.to_i, datefin
		}
		
		vboxavancement.pack_start Gtk::Label.new("Icônes:"), :expand => false, :fill => false, :padding => 3
		
		box_icones = Gtk::Box.new :horizontal, 2
		box_icones.pack_start @icone1, :expand => true, :fill => true, :padding => 1
		box_icones.pack_start @icone2, :expand => true, :fill => true, :padding => 1
		vboxavancement.pack_start box_icones, :expand => false, :fill => false, :padding => 3
		
		vboxavancement.pack_start @modifavancement, :expand => true, :fill => true, :padding => 3
		
		vbox.pack_start( hbox1, :expand => false, :fill => false, :padding => 2) 
		
		hbox2 = Gtk::Box.new :horizontal, 2
		vboxdesc = Gtk::Box.new :vertical, 2
		aligndesc = Gtk::Alignment.new 0, 0, 0, 0
		aligndesc.add Gtk::Label.new( "Description :" )
		vboxdesc.pack_start( aligndesc, :expand => false, :fill => false, :padding => 3 )
		vboxdesc.pack_start @description, :expand => true, :fill => true, :padding => 3
		vboxinfo.pack_start( vboxdesc, :expand => true, :fill => true, :padding => 3 )
		
		vbox.pack_start( hbox2, :expand => false, :fill => false, :padding => 3 ) 
		
		vbox

	end
	
	def remplir_icones combo, icone
	
		combo.model.clear
		
		num = 0
		i=0
		Dir.glob(File.join("./resources/icones/", "*.png")).sort.each { |f| 
			iter = combo.model.append
			iter[0] = (File.basename(f)=="000.png" ? "" : File.basename(f))
			iter[1] = Gdk::Pixbuf.new(File.join("./resources/icones/",File.basename(f).to_s), 22, 22)
			num = i if icone==File.basename(f)
			i+=1
		}
		
		combo.active = num
	
	end
	
	def boutons
	
		align = Gtk::Alignment.new 1, 1, 0, 0
		
		hbox = Gtk::Box.new :horizontal
		
		@suppr = Gtk::Button.new
		hboxsuppr = Gtk::Box.new :horizontal
		hboxsuppr.add Gtk::Image.new( :file => "./resources/icons/remove.png" )
		hboxsuppr.add Gtk::Label.new "Supprimer action"
		@suppr.add hboxsuppr
		
		@suppr.sensitive = false
		
		@suppr.signal_connect ("clicked") {
			supprime(@view.model.get_value @view.selection.selected, 0)
		}
		
		ajout = Gtk::Button.new
		hboxajout = Gtk::Box.new :horizontal
		hboxajout.add Gtk::Image.new( :file => "./resources/icons/add.png" )
		hboxajout.add Gtk::Label.new "Nouvelle action"
		ajout.add hboxajout
		
		ajout.signal_connect ("clicked") {
			@window.get_nouvelle_action.change @id_travaux
			@window.affiche @window.get_nouvelle_action
		}
		
		hbox.pack_start(@suppr, :expand => false, :fill => false, :padding => 2)
		hbox.pack_start(ajout, :expand => false, :fill => false, :padding => 2)
		
		align.add hbox
		
		align
	
	end
	
	def refresh id_travaux
	
		@id_travaux = id_travaux
		
		res = Travaux.find(id_travaux)
		
		remplir_travail res
		
		res = Actions.where(:suppr => 0, :id_trav =>id_travaux).order(:date)
		
		@suppr.sensitive = false
		@modifavancement.sensitive = true
		
		remplir_actions res
	
	end
	
	def tableau_travaux
	
		@list_store = Gtk::TreeStore.new(Integer, String, String, String, Integer, String)
		@view = Gtk::TreeView.new(@list_store)
		@view.signal_connect ("row-activated") { |view, path, column|
			#p @view.model.get_value @view.selection.selected, 0
		}
		
		@view.signal_connect ("cursor-changed") {
			@suppr.sensitive = true
		}
		
		# Create a renderer with the background property set
		renderer_left = Gtk::CellRendererText.new
		renderer_left.background = "white"
		renderer_left.xalign = 0
		
		# Create a renderer with the background property set
		renderer_center = Gtk::CellRendererText.new
		renderer_center.background = "white"
		renderer_center.xalign = 0.5
		
		# Create a renderer with the background property set
		renderer_center_bold = Gtk::CellRendererText.new
		renderer_center_bold.background = "white"
		renderer_center_bold.xalign = 1
		renderer_center_bold.weight = Pango::WEIGHT_HEAVY
		
		renderer_progress = Gtk::CellRendererProgress.new
		
		renderer_pix = Gtk::CellRendererPixbuf.new
		
		col = Gtk::TreeViewColumn.new("Date", renderer_left, :text => 5)
		col.sort_column_id = 1
		col.resizable = true
		@view.append_column(col)
		
		col = Gtk::TreeViewColumn.new("Description", renderer_left, :text => 2)
		col.sort_column_id = 2
		col.resizable = true
		col.expand = true
		@view.append_column(col)
		
		col = Gtk::TreeViewColumn.new("Auteur", renderer_left, :text => 3)
		col.sort_column_id = 3
		col.resizable = true
		@view.append_column(col)
		
		scroll = Gtk::ScrolledWindow.new
  	scroll.set_policy(:automatic, :always)
  	scroll.add @view
  	
  	scroll
	
	end
	
	def remplir_actions res
		@list_store.clear
		
		ajout = Gdk::Pixbuf.new("resources/icons/add.png", 22, 22)
		modification = Gdk::Pixbuf.new("resources/icons/saisie.png", 22, 22)
		suppression = Gdk::Pixbuf.new("resources/icons/remove.png", 22, 22)
		
		res.each { |h| 
			
			iter = @list_store.append nil
			iter[0] = h.id
			iter[1] = h.date.strftime("%Y%m%d")
			iter[2] = h.description
			iter[3] = "#{h.users.firstname} #{h.users.lastname}"
			iter[5] = h.date.strftime("%d/%m/%Y")
		}
			
	end
	
	def remplir_travail h

		auteur_id = ""
		datefin = ""
		@info1.text = (h.info1.nil? ? "" : h.info1)
		@info2.text = (h.info2.nil? ? "" : h.info2)
		@description.deserialize(h.description_tag, h.description)
		@avancement.value = h.avancement.to_i
		auteur_id = h.users_id
		datefin = h.datefin
		@datecreation = h.datedeb
		@dateecheance.select_month datefin.month, datefin.year
		@dateecheance.select_day datefin.day
		
		droit_modif = (auteur_id.to_i.eql?(@user.id) or @user.admin)
		@info1.editable = droit_modif
		@info2.editable = droit_modif
		@description.notepad.editable = droit_modif
		@avancement.sensitive = droit_modif
		@dateecheance.sensitive = droit_modif
		
		@frametravail.label = "Travail (date de création : #{h.datedeb.strftime("%d/%m/%Y")})"
		
		remplir_icones @icone1, h.icone1
		remplir_icones @icone2, h.icone2
	
	end
	
	def supprime id
	
		dialog = Gtk::MessageDialog.new(:parent => @window,
				                            :flags => :destroy_with_parent,
				                            :type => :question,
				                            :buttons_type => :yes_no,
				                            :message => "Voulez-vous réellement supprimer cette action ?")
		response = dialog.run
		case response
		  when Gtk::ResponseType::YES
				a = Actions.find(id)
				a.suppr = 1
				a.save
				refresh @id_travaux
		end
		dialog.destroy
	
	end
	
	def modif_travail id, info1, info2, description, tag, avancement, datefin

		if datefin>=@datecreation then
			
			travail = Travaux.find(id)
			travail.info1 = info1
			travail.info2 = info2
			travail.description = description
			travail.description_tag = tag
			travail.avancement = avancement
			travail.datefin = datefin
			travail.icone1 = @icone1.active_iter[0] if @icone1.active>-1
			travail.icone2 = @icone2.active_iter[0] if @icone2.active>-1
		
			travail.save
	
		else
			dialog = Gtk::MessageDialog.new(:parent => @window, 
						                          :flags => :destroy_with_parent,
						                          :type => :error,
						                          :buttons_type => :ok,
						                          :message => "La date d'échéance est antérieure à la date de création !\n Ce travail a été créé le #{@datecreation}")
		dialog.run
		dialog.destroy
		end
	
	end
	
end
