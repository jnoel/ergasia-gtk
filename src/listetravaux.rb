# coding: utf-8

class ListeTravaux < Gtk::Box

	def initialize window
	
		super(:vertical, 3)
		
		set_border_width 10
		
		@window = window
		@login = window.get_login
		
		@statut = 1
		
		agencement
	
	end
	
	def agencement
	
		@frame = Gtk::Frame.new
		box_frame = Gtk::Box.new :vertical, 3
		box_recherche = Gtk::Box.new :horizontal, 2
		box_recherche.pack_start Gtk::Label.new("Rechercher :"), :expand => false, :fill => false, :padding => 2
		@search = Gtk::Entry.new
		@search.signal_connect( "activate" ) {
			@icone.active = -1
			refresh @statut, @search.text
		}
		box_recherche.pack_start @search, :expand => false, :fill => false, :padding => 2
		box_recherche.pack_start Gtk::Label.new("Icônes :"), :expand => false, :fill => false, :padding => 2
		@icone = Gtk::ComboBox.new :model => Gtk::ListStore.new(String, Gdk::Pixbuf)
		renderer_pix = Gtk::CellRendererPixbuf.new		
		@icone.pack_start(renderer_pix, true)
		@icone.set_attributes(renderer_pix, :pixbuf => 1)
		remplir_icones @icone
		@icone.signal_connect("changed") {
			if @icone.active_iter
				ic = (@icone.active>0 ? @icone.active_iter[0] : "")
				refresh @statut, text=@search.text, icone=ic
			end
		}
		box_recherche.pack_start @icone, :expand => false, :fill => false, :padding => 2
		box_frame.pack_start box_recherche, :expand => false, :fill => false, :padding => 2
		box_frame.pack_start tableau_travaux, :expand => true, :fill => true, :padding => 2
		@frame.add box_frame
		
		self.pack_start @frame, :expand => true, :fill => true, :padding => 3
	
	end
	
	def remplir_icones combo
	
		combo.model.clear

		Dir.glob(File.join("./resources/icones/", "*.png")).sort.each { |f| 
			iter = combo.model.append
			iter[0] = (File.basename(f)=="000.png" ? "" : File.basename(f))
			iter[1] = Gdk::Pixbuf.new(File.join("./resources/icones/",File.basename(f).to_s), 22, 22)
		}
	
	end
	
	def refresh statut, text=nil, icone=nil
	
		@statut = statut
		
		res = Travaux.where(:suppr => 0).order(:datefin)
		
		case statut
		when 1
		   @frame.label = "Travaux en cours"
		   res = res.where("avancement<?", 100)
		when 2
		   @frame.label = "Travaux en retard"
		   res = res.where("avancement<? AND datefin<?", 100, Date.today)
		else
		   @frame.label = "Travaux archivés"
		   res = res.where(:avancement => 100)
		end
		
		res = res.where("info1 ILIKE ? OR info2 ILIKE ? OR description ILIKE ?", "%#{text}%", "%#{text}%", "%#{text}%") unless text.to_s.empty?
		res = res.where("icone1=? OR icone2=?", icone, icone) unless icone.to_s.empty?
		
		remplir res
	
	end
	
	def tableau_travaux
	
		# list_store (id, info1, info1, description, auteur, date création, date échéance, avancement, date-tri, echéance-tri)
		@list_store = Gtk::TreeStore.new(Integer, String, String, String, String, String, String, Integer, String, String, String, Gdk::Pixbuf, Gdk::Pixbuf)
		@view = Gtk::TreeView.new(@list_store)
		@view.rules_hint = true
		@view.signal_connect ("row-activated") { |view, path, column|
			#p @view.model.get_value @view.selection.selected, 0
			@window.get_actions.refresh(@view.model.get_value @view.selection.selected, 0)
			@window.affiche @window.get_actions
		}
		
		# Create a renderer with the background property set
		renderer_left = Gtk::CellRendererText.new
		#renderer_left.background = "white"
		renderer_left.xalign = 0
		
		# Create a renderer with the background property set
		renderer_center = Gtk::CellRendererText.new
		#renderer_center.background = "white"
		renderer_center.xalign = 0.5
		
		# Create a renderer with the background property set
		renderer_center_bold = Gtk::CellRendererText.new
		#renderer_center_bold.background = "white"
		renderer_center_bold.xalign = 1
		renderer_center_bold.weight = Pango::WEIGHT_HEAVY
		
		renderer_progress = Gtk::CellRendererProgress.new	
		
		renderer_pix = Gtk::CellRendererPixbuf.new	
		
		col = Gtk::TreeViewColumn.new(@window.get_info(1), renderer_left, :text => 1, :background => 8)
		col.sort_column_id = 1
		col.resizable = true
		@view.append_column(col) unless @window.get_info(1).empty?
		
		col = Gtk::TreeViewColumn.new(@window.get_info(2), renderer_left, :text => 2, :background => 8)
		col.sort_column_id = 2
		col.resizable = true
		@view.append_column(col) unless @window.get_info(2).empty?
		
		col = Gtk::TreeViewColumn.new("Description", renderer_left, :text => 3, :background => 8)
		col.sort_column_id = 3
		col.resizable = true
		col.expand = true
		col.max_width = 20
		@view.append_column(col)
		
		col = Gtk::TreeViewColumn.new("Auteur", renderer_left, :text => 4, :background => 8)
		col.sort_column_id = 4
		col.resizable = true
		@view.append_column(col)
		
		col = Gtk::TreeViewColumn.new("Date de création", renderer_center, :text => 9, :background => 8)
		col.sort_column_id = 5
		col.resizable = true
		@view.append_column(col)
		
		col = Gtk::TreeViewColumn.new("Date d'échéance", renderer_center, :text => 10, :background => 8)
		col.sort_column_id = 6
		col.resizable = true
		@view.append_column(col)
		
		col = Gtk::TreeViewColumn.new
		col.title = "Avancement"
		col.pack_start(renderer_progress, true)
		col.add_attribute(renderer_progress, 'value', 7)
		col.sort_column_id = 7
		col.resizable = true
		@view.append_column(col)
		
		col = Gtk::TreeViewColumn.new("", renderer_pix, :pixbuf => 11)
		@view.append_column(col)
		
		col = Gtk::TreeViewColumn.new("", renderer_pix, :pixbuf => 12)
		@view.append_column(col)
		
		scroll = Gtk::ScrolledWindow.new
  	scroll.set_policy(:automatic, :always)
  	scroll.add @view
  	
  	scroll
	
	end
	
	def remplir res
		# TODO
		@list_store.clear
		
		ajout = Gdk::Pixbuf.new("resources/icons/add.png", 22, 22)
		modification = Gdk::Pixbuf.new("resources/icons/saisie.png", 22, 22)
		suppression = Gdk::Pixbuf.new("resources/icons/remove.png", 22, 22)
		
		res.each { |h| 
			
			iter = @list_store.append nil
			iter[0] = h.id
			iter[1] = h.info1
			iter[2] = h.info2
			iter[3] = h.description
			iter[4] = "#{h.users.firstname} #{h.users.lastname}"
			iter[5] = h.datedeb.strftime("%Y%m%d")
			iter[6] = h.datefin.strftime("%Y%m%d")
			iter[7] = h.avancement
			diff = (h.datefin-Date.today).to_i
			iter[8] = ( diff>7 ? "#ffffff" : ( diff<0 ? "#ffcece" : "#ffeace" ) )
			iter[9] = h.datedeb.strftime("%d/%m/%Y")
			iter[10] = h.datefin.strftime("%d/%m/%Y")
			begin
				iter[11] = Gdk::Pixbuf.new(File.join("./resources/icones/",h.icone1), 22, 22) unless h.icone1.to_s.empty?
				iter[12] = Gdk::Pixbuf.new(File.join("./resources/icones/",h.icone2), 22, 22) unless h.icone2.to_s.empty?
			rescue
				puts "Impossible de charger une icone"
			end
		}
	
	end
	
end
