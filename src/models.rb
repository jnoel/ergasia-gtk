# coding: utf-8

class DB < ActiveRecord::Base
	self.abstract_class = true
	self.pluralize_table_names = false
end

class Travaux < DB
	belongs_to :users
	has_many :actions
end

class Actions < DB
	belongs_to :travaux
	belongs_to :users
end

class Actions_predefinies < DB

end

class Configuration < DB

end

class Users < DB
	has_many :travaux
	has_many :actions
end
