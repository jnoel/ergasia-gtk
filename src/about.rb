# coding: utf-8
		
class About < Gtk::AboutDialog

	def initialize
		
		super

		self.version = "0.9.5"
		self.comments = "Logiciel de suivi de travaux"
		lic = ""
		file = File.open("../LICENCE", "r")
		while (line=file.gets)
			lic += line
		end
		file.close
		self.license = lic
		self.website = "http://www.mithril.re"
		self.authors = ["Mithril Informatique", "Jean-Noël Rouchon"]
		self.artists = ["Faience icon theme"]
		self.logo = Gdk::Pixbuf.new( "./resources/icons/icon.png" )
		self.set_icon( "./resources/icons/icon.png" )
		
		self.show
	
	end

end		
