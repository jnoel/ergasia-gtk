# coding: utf-8

class Login < Gtk::Table
	
	def initialize window
	
		super( 3, 3, false )
		
		@window = window
		@user = User.new
		conf = window.get_conf
		
		self.attach( Gtk::Image.new( :file => "./resources/images/logo.png" ), 0, 3, 0, 1 )
		
		align0 = Gtk::Alignment.new 1, 1, 0, 0
		align0.add Gtk::Label.new( "Utilisateur :" )
		self.attach( align0, 0, 1, 1, 2, :fill, :fill, 5, 5 )
		
		align1 = Gtk::Alignment.new 1, 1, 1, 0
		@utilisateur = Gtk::Entry.new
		align1.add @utilisateur
		self.attach( align1, 1, 2, 1, 2, :fill, :fill, 5, 5 )
		
		align3 = Gtk::Alignment.new 1, 0, 0, 0
		align3.add Gtk::Label.new( "Mot de passe :" )
		self.attach( align3, 0, 1, 2, 3, :fill, :fill, 5, 5 )
		
		align4 = Gtk::Alignment.new 1, 0, 1, 0
		mdp = Gtk::Entry.new
		mdp.visibility = false
		align4.add mdp
		self.attach( align4, 1, 2, 2, 3, :fill, :fill, 5, 5 )
		
		validebt = Gtk::Button.new
		validebt.image = Gtk::Image.new( :file => "./resources/icons/validate.png" )
		align2 = Gtk::Alignment.new 0, 0.5, 0, 0
		align2.add validebt
		self.attach( align2, 2, 3, 1, 3, :fill, :fill, 5, 5 )
		
		validebt.signal_connect( "clicked" ) {
			verif @utilisateur.text, mdp.text
		}
		
		@utilisateur.signal_connect( "activate" ) {
			verif @utilisateur.text, mdp.text
		}
		
		mdp.signal_connect( "activate" ) {
			verif @utilisateur.text, mdp.text
		}
		
		@utilisateur.text = conf["divers"]["default_user"] if conf["divers"]["default_user"]
		mdp.text = conf["divers"]["default_user_password"] if conf["divers"]["default_user_password"]
			
	end
	
	def verif util, mdp
	
		if !util.empty? then
		
			res = Users.where(:username => util, :active => 1)
		
			password = ""
			res.each { |h| 
				@user.id = h.id
				@user.firstname = h.firstname
				@user.lastname = h.lastname
				@user.username = h.username
				@user.admin = (h.admin == 1)
				@user.email = h.email
				password = h.password
			}
		
			#if res.num_rows>0 then
			if res.count>0 then
				if ( BCrypt::Password.new(password) == mdp ) then 
					log
				else
					log_fail
				end
			else
				log_fail
			end
		end
	
	end
	
	def log_fail
	
		dialog = Gtk::MessageDialog.new(:parent => @window, 
			                        			:flags => :destroy_with_parent,
			                        			:type => :warning,
			                        			:buttons_type => :ok,
			                        			:message => "Le nom ou le mot de passe ne corresponde pas !")
		dialog.run
		dialog.destroy
	
	end
	
	def log
	
		@window.active_menu
		@window.affiche @window.get_listetravaux
		@window.get_listetravaux.refresh 1
		@window.maximize
		@window.get_listeusers.get_ajoutbt.sensitive = @user.admin
	
	end
	
	def get_user
		@user
	end
	
	def focus
		@utilisateur.grab_focus
	end
	
end

class User

	attr_accessor :id, :firstname, :lastname, :username, :admin, :email

end
