# coding: utf-8

class Editeur < Gtk::Box

	attr :notepad

	def initialize
	
		super(:vertical)
		
		@notepad = notepad
		
		self.pack_start toolbar, :expand => false, :fill => false, :padding => 2
		self.pack_start scroll, :expand => true, :fill => true, :padding => 2
	
	end
	
	def toolbar
		toolb = Gtk::Toolbar.new
		toolb.set_toolbar_style Gtk::Toolbar::Style::ICONS
		
		bt_efface = Gtk::ToolButton.new( :icon_widget => Gtk::Image.new( :file => "./resources/editeur/edit-clear.png" ), :label => "Effacer" )
		bt_italique = Gtk::ToolButton.new( :icon_widget => Gtk::Image.new( :file => "./resources/editeur/format-text-italic.png" ), :label => "Italique" )
		bt_gras = Gtk::ToolButton.new( :icon_widget => Gtk::Image.new( :file => "./resources/editeur/format-text-bold.png" ), :label => "Gras" )
		bt_souligne = Gtk::ToolButton.new( :icon_widget => Gtk::Image.new( :file => "./resources/editeur/format-text-underline.png" ), :label => "Souligné" )
		bt_barre = Gtk::ToolButton.new( :icon_widget => Gtk::Image.new( :file => "./resources/editeur/format-text-strikethrough.png" ), :label => "Barré" )
		bt_couleurs = Gtk::ToolButton.new( :icon_widget => Gtk::Image.new( :file => "./resources/editeur/preferences-color.png" ), :label => "Couleurs" )
				
		bt_efface.signal_connect("clicked") { 
			sel = @notepad.buffer.selection_bounds
			if sel[2]
				@notepad.buffer.remove_all_tags(sel[0], sel[1])
			end
		}
		
		bt_italique.signal_connect("clicked") { 
			sel = @notepad.buffer.selection_bounds
			if sel[2]
				@notepad.buffer.apply_tag("italique", sel[0], sel[1])
			end
		}
		
		bt_gras.signal_connect("clicked") { 
			sel = @notepad.buffer.selection_bounds
			if sel[2]
				@notepad.buffer.apply_tag("gras", sel[0], sel[1])
			end
		}
		
		bt_souligne.signal_connect("clicked") { 
			sel = @notepad.buffer.selection_bounds
			if sel[2]
				@notepad.buffer.apply_tag("souligne", sel[0], sel[1])
			end
		}
		
		bt_barre.signal_connect("clicked") { 
			sel = @notepad.buffer.selection_bounds
			if sel[2]
				@notepad.buffer.apply_tag("barre", sel[0], sel[1])
			end
		}
		
		bt_couleurs.signal_connect("clicked") { 
			sel = @notepad.buffer.selection_bounds
			if sel[2]
				dialogue = Gtk::ColorSelectionDialog.new
				dialogue.modal = true
				dialogue.show_all
				dialogue.signal_connect('response') { |w, response|  
					if response==Gtk::ResponseType::OK
      			colorsel = dialogue.color_selection
      			color = colorsel.current_color
      			@notepad.buffer.create_tag color.to_s, {"foreground" => color.to_s}
      			@notepad.buffer.apply_tag(color.to_s, sel[0], sel[1])
      		end
      		dialogue.destroy
				}
			end
		}
		
		tools = [bt_efface, Gtk::SeparatorToolItem.new, bt_italique, bt_gras, bt_souligne, bt_barre, Gtk::SeparatorToolItem.new, bt_couleurs]
        
    tools.each_index { |i| 
    	toolb.insert tools[i], i
    }
		
		return toolb
	end
	
	def notepad		
		textview = Gtk::TextView.new
		textview.buffer.create_tag "italique", {"style" => Pango::FontDescription::STYLE_ITALIC}
		textview.buffer.create_tag "souligne", {"underline" => Pango::AttrUnderline::SINGLE}
		textview.buffer.create_tag "barre", {"strikethrough" => true}
		textview.buffer.create_tag "gras", {"weight" => Pango::FontDescription::WEIGHT_BOLD}
		textview.buffer.create_tag "rouge", {"background" => "red"}
		textview.buffer.create_tag "rouge_italique", {"background" => "red", "style" => Pango::FontDescription::STYLE_ITALIC}
		textview.wrap_mode = :word
		
		return textview
	end
	
	def scroll
		scrolldesc = Gtk::ScrolledWindow.new
		scrolldesc.add @notepad
		scrolldesc.set_policy :automatic, :automatic
		scrolldesc.shadow_type = :in
		
		return scrolldesc
	end
	
	def serialize
		txtbuffer = @notepad.buffer
		serialFormat = txtbuffer.register_deserialize_target(nil)
		data = txtbuffer.serialize(txtbuffer, serialFormat, txtbuffer.start_iter, txtbuffer.end_iter)
		tag = ""
		data.bytes.to_a.each do |i|
			tag << "#{i},"
		end
		tag.chop!
		return tag
	end
	
	def deserialize(data, recup)
		txtbuffer = Gtk::TextBuffer.new @notepad.buffer.tag_table
		serialFormat = txtbuffer.register_deserialize_target(nil)
		begin
			str = ""
			data.split(",").to_a.each do |car|
				str << car.to_i.chr
			end
			coul = []
			i = 0
			while !i.nil? do
				i = str.index('<tag name=', i+1)
				if i
					if str[i+11]=="#"
						coul << str[i+11..i+23]
					end
				end
			end
			coul.each do |c|
				@notepad.buffer.create_tag c, {"foreground" => c}
			end
			txtbuffer.deserialize(txtbuffer, serialFormat, txtbuffer.start_iter, str)
		rescue
			txtbuffer.text = recup.to_s
		end
		@notepad.buffer = txtbuffer
	end
	
	def get_text
		return @notepad.buffer.text
	end
	
	def set_text text
		@notepad.buffer.text = text.to_s
	end

end
