# encoding: UTF-8

def mail(to,opts={})
  opts[:address]     ||= 'smtp.orange.fr'
  opts[:user]      	 ||= ''
  opts[:secret]      ||= ''
  opts[:port]        ||= '25'
  opts[:ssl]         ||= false
  opts[:authtype]    ||= :plain
  opts[:from]        ||= ''
  opts[:from_alias]  ||= ''
  opts[:subject]     ||= ""
  opts[:body]        ||= ""

  msg = <<END_OF_MESSAGE
From: #{opts[:from_alias]} <#{opts[:from]}>
To: <#{to.join(",")}>
Subject: #{opts[:subject]}
date: #{DateTime.now.strftime "%a, %d %b %Y %H:%M:%S %z"}

#{opts[:body]}
END_OF_MESSAGE

	smtp = Net::SMTP.new opts[:address], opts[:port]
	smtp.enable_ssl if opts[:ssl]
  smtp.start(opts[:domain], opts[:user], opts[:secret], opts[:authtype]) do |smtp|
    smtp.send_message msg, opts[:from], to
  end
end
