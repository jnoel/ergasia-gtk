# coding: utf-8

class NouvelleAction < Gtk::Box

	def initialize window
	
		super(:vertical, 3)
		
		set_border_width 10
		
		@window = window
		@login = window.get_login
		
		@id_trav = -1
		
		@predefini = Gtk::ComboBoxText.new
		@description = Editeur.new
		@datecreation = Gtk::Calendar.new

		@valider = Gtk::Button.new :stock_id => Gtk::Stock::OK
		@annuler = Gtk::Button.new :stock_id => Gtk::Stock::CANCEL
		
		agencement
		
		remplir_combo
		
		@valider.signal_connect( "clicked" ) { validate }
		@annuler.signal_connect( "clicked" ) { cancel }
		@predefini.signal_connect ( "changed" ) { @description.set_text @predefini.active_text unless @predefini.active_text.nil? }
	
	end
	
	def agencement
	
		hbox2 = Gtk::Box.new :horizontal, 2
		vboxdesc = Gtk::Box.new :vertical, 2
		aligndesc = Gtk::Alignment.new 0, 0, 0, 0
		aligndesc.add Gtk::Label.new( "Description :" )
		vboxdesc.pack_start( aligndesc, :expand => false, :fill => false, :padding => 3 )
		vboxdesc.pack_start( @predefini, :expand => false, :fill => false, :padding => 3 )
		vboxdesc.pack_start( @description, :expand => true, :fill => true, :padding => 3 )

		hbox2.pack_start( vboxdesc, :expand => true, :fill => true, :padding => 3 )

		vbox2 = Gtk::Box.new :vertical, 2
		align3 = Gtk::Alignment.new 1, 0, 0, 0
		align3.add vbox2
		vbox2.add Gtk::Label.new( "Date de l'action :" )
		vbox2.add @datecreation
		hbox2.pack_start( align3, :expand => false, :fill => false, :padding => 3 )
		
		hbox3 = Gtk::Box.new :horizontal, 2
		align1 = Gtk::Alignment.new 1, 1, 0, 0
		hbox3.pack_start( @annuler, :expand => true, :fill => true, :padding => 3 )
		hbox3.pack_start( @valider, :expand => true, :fill => true, :padding => 3 )
		align1.add hbox3
		
		vboxframe = Gtk::Box.new :vertical, 2
		vboxframe.pack_start hbox2, :expand => true, :fill => true, :padding => 3
		
		frame = Gtk::Frame.new "Nouvelle action"
		frame.add vboxframe
		
		pack_start frame, :expand => true, :fill => true, :padding => 3
		pack_start align1, :expand => false, :fill => false, :padding => 3
	
	end
	
	def remplir_combo
	
		res = Actions_predefinies.order(:id)
		
		res.each do |a|
			@predefini.append_text a.description
		end	
	
	end
	
	def validate
		
		datedeb = @datecreation.year.to_s + "-" + (@datecreation.month+1).to_s + "-" + @datecreation.day.to_s
		
		if !@description.get_text.empty? then
			Actions.create :id_trav => @id_trav, :date => Date.parse(datedeb), :description => @description.get_text, :users_id => @login.get_user.id 
			quit 1
		else
			erreur = ""
			erreur += "La description n'est pas renseignée.\n" if @description.get_text.empty?
			dialog = Gtk::MessageDialog.new :parent => @window, :flags => :modal, :type => :error, :buttons_type => :close, :message => erreur
			dialog.title = "Erreur"
			dialog.signal_connect('response') { dialog.destroy }
    	dialog.run
		end
	
	end
	
	def cancel
	
		quit
	
	end
	
	def quit statut=nil
	
		@description.set_text ""
		@datecreation.select_month Date.today.month, Date.today.year
		@datecreation.select_day Date.today.day
		@window.affiche @window.get_actions
		@window.get_actions.refresh @id_trav unless statut.nil?
	
	end
	
	def change id_trav
		@id_trav = id_trav
		@predefini.active = -1
	end
	
end
