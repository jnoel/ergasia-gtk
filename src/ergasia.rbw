#!/usr/bin/env ruby
# coding: utf-8

require 'rubygems'
require 'bundler/setup'
require 'gtk3'
require 'date'
require 'bcrypt'
require 'yaml'
require 'active_record'
require 'net/smtp'

Dir.foreach('.') { |f|
	require "./#{f}" if File.extname(f).eql?(".rb")
}

GLib.set_application_name "Ergasia"

Gtk.init
win = MainWindow.new :toplevel
Gtk.main
